use warnings;

## Read the file which gives the mapping from the file name 
## to sample name. It is assumed that the file is tab delimited
## 1st column is the file name
## 2nd column is the sample name
my $names_file = $ARGV[0];
my $adaptor_file = $ARGV[1];
my $barcode_idx = $ARGV[2];

print "Barcode Index: $barcode_idx";

open(OPENFILE1, $names_file);
my %file_hash = ();
##Create a has where the file name is the key and the sample name
##is the value 
while(<OPENFILE1>){
	chomp;
	($file_name, $sample_name) = split("\t", $_);
	$file_hash{ $file_name }	= $sample_name;
}
close(OPENFILE1);

## Read the file with all the illumina adaptors and create a hash that
## keys on the barcode and the value is the index of the adaptor
open(OPENFILE2, $adaptor_file);
my %adaptor_hash = ();
while(<OPENFILE2>){
	chomp;
	($barcode, $index) = split("\t", $_);
	$adaptor_hash{ $barcode } = $index;
}
close(OPENFILE2);

## Get the fastq.gz file
my $fastq = "*.fastq.gz";
my @fastq_files = glob $fastq;

foreach(@fastq_files){
	$file_name = $_;
	print "Performing fastqc on the file\n";
	system("fastqc", $file_name);
	##Get the barcode from the file name
	@file_parts = split(/_/,$file_name);
	$file_barcode = $file_parts[$barcode_idx];
	print "File Barcode: $file_barcode\n";
	## Get the index for the barcode
	if( exists($adaptor_hash{ $file_barcode }) ){
		$index = $adaptor_hash{ $file_barcode };
	} else {
		$index = 0;
	}
	## Get the name of the sample name corresponding to
	## the file name. If there is no sample name then
	## it takes everything before the barcode from the 
	#file name
	if( exists($file_hash{ $file_name })){
		$sample_name = $file_hash{ $file_name };
	} else {
		$sample_name = $file_parts[$barcode_idx - 1];
	}
	print "$index\n";
	print "$sample_name\n";
	print "Quality trimming - Adaptor Trimming - As trimming\n";
	##/ifs/e63data/leslielab/singh/apacalypse/Rpkgs/TagSeq/inst/scripts/multiplex-preprocess
	system("multiplex-preprocess", $index ,$file_name,"$sample_name.fastq");
	print "Gziping the fastq file\n";
	system("gzip", "$sample_name.fastq");
	print "BWA alignment\n";
	system "bwa aln -t 24 -l 100000 -n 0.04 -q 0 \$BWA_INDEXES $sample_name.fastq.gz > $sample_name.sai";
	print "SAI to SAM\n";
	system "bwa samse -n 5 \$BWA_INDEXES $sample_name.sai $sample_name.fastq.gz > $sample_name.sam";
	print "Adding XX\n";
	system "perl ~/scripts/addXX.pl $sample_name.sam";
	print "SAM to BAM with sorting\n";
	system "samtools view -bS $sample_name.sam |  samtools sort - $sample_name";
	print "Reheader of BAM\n";
	system "samtools reheader /ifs/e63data/leslielab/singh/apa/raw_data/2013-09-13/PL2-1_5-1/PL2-1_5-1.sam $sample_name.bam > $sample_name-h.bam";
	system("mv", "$sample_name-h.bam", "$sample_name.bam");
	print "Indexing BAM file\n";
	system "samtools index $sample_name.bam";
	print "Doing Tag distribution\n";
	system "/ifs/e63data/leslielab/singh/usr/local/bin/Rscript /ifs/e63data/leslielab/singh/apa/raw_data/preprocessing_scripts/tagseq-qc-distro -t 24 -d /ifs/e63data/leslielab/singh/annotation_data/hg19 $sample_name.bam";
}



















