#!/bin/sh
# 
# example script for submitting an array job to gridware. 
# #$ precedes parameters given to qsub
# I have included those I thought were most useful 
# you may not want to use all of them.
#
#  clear SGE defaults
#$ -clear
# Specifies the interpreting shell for the job.
#$ -S /bin/sh
# Execute the job from  the  current  working  directory.
#$ -cwd

# specifies the list of queues which may be used to execute this job. 
# 	if your jobs are long you may not want to monopolize the cluster.
#$ -q all.q,big.q

# Identifies the ability of a job to be rerun or not.  If
#          the  value  of  -r is 'y', rerun the job if the job was
#          aborted without leaving a consistent exit  state  (this
#          is  typically  the case if the node on which the job is
#          running crashes).  If -r is 'n', do not rerun  the  job
#          under any circumstances.
#$ -r n

# The default amount of memory for a job is 2G
# To increase the memory limit, use option -l h_vmem=<gbsize>GB.
# The default number of threads or cores for each job is 1
# If your job uses more than one thread you need to specify a parallel environment
# To increase the number of threads, use option -pe smb <nrthreads>.
# Please note that the total amount of memory available for your job is gbsize*nrthreads GB. 
#$ -l  h_vmem=4G
#$ -pe smp 10


# the name of the job, shows up in qstat

# Defines the priority of the job relative to other jobs in the queue. 
# 	Priority is an integer in the range -1023 to 1024. If you want 
#	to let other jobs go in front of yours in the queue you can set
#	it lower. The default is 0. 
#$ -p 0

# Submits a so called Array Job, i.e. an array of identi-
#          cal  tasks being only differentiated by an index number
#          and being treated by Grid Engine almost like a series
#          of jobs. The option argument to -t specifies the number
#          of array job tasks and the index number which  will  be
#          associated  with  the  tasks. The index numbers will be
#          exported to the job tasks via the environment  variable
#          SGE_TASK_ID.   
#$ -t 1-1

# merge std error and std out into one file
#$ -j y

#STDOUT and STDERR of array job tasks  will  be  written
#          into different files with the locations
#          <jobname>.['e'|'o']<job_id>'.'<task_id>
#	This can get screwy if you are writing a lot to stdout.
#	But if you have a lot of jobs it will prevent you from
#	having a file for each one. You can give these any name.


# the variable $tasks holds the inputs for your script. myscriptfoo.pl would
# be the wrapper script that does your real work.  Each time 
# myscriptfoo.pl is called it gets one of the values from tasks 
# and is assigned a SGE_TASK_ID (defined above in -t) as ja-task-ID
# that you see in qstat.

# The following are the command line arguments to the run_alignments scripts
# Example invocation: qsub run_alignments.sh bowtie2 hg19 reads_R1_fastq.gz TRUE

sample_names_file=$1
adaptors_file=$2
position_of_adaptor=$3
. ~/.bash_profile
perl /ifs/e63data/leslielab/singh/apa/raw_data/preprocessing_scripts/preprocessing.pl ${sample_names_file} ${adaptors_file} ${position_of_adaptor}



