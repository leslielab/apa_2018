This repository provides the set of packages that were used for performing peak calls of 3-seq data and then filtering them to provide the robust set of APA events in the 3'UTR and introns. 
These packages were tested with R version 3.4.3 and Bioconductor version 3.6. TagSeq and Biosignals were build by contributions from Dr. Steve Lianoglou, Dr. Yuheng Lu and Dr. Irtisha Singh. 
The Biostrings package is from Bioconductor with some modifications to accomdate the requirements for flagging the peaks with required annotations. 

Peak calling was done on bam files (aligned by BWA) followed by quantification. The input for both these steps are yaml files, identify-peaks.yaml and quantify-peaks.yaml (examples are under sample_run).

Follow the steps in analysis_all_steps.R (under sample_run) to go from bam files to an atlas of robust APA events.

All the steps were best configured for genome hg19. Please direct any questions to is327@cornell.edu

