# README #

This is the master package for ApA/CLIP-Seq data processing pipelines.

### Dependencies ###

Integrated relevant functions from SeqTools (https://bitbucket.org/leslielab/seqtools) and GenomicCache (https://bitbucket.org/leslielab/genomiccache) packages. Still rely on biosignals (https://bitbucket.org/leslielab/biosignals) package for coverage peak calling. 